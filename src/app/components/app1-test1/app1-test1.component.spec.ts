import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { App1Test1Component } from './app1-test1.component';

describe('App1Test1Component', () => {
  let component: App1Test1Component;
  let fixture: ComponentFixture<App1Test1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ App1Test1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(App1Test1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
