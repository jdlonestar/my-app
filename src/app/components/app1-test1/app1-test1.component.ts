import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { singleSpaPropsSubject, SingleSpaProps } from 'src/single-spa/single-spa-props';
import { Subscription, Observable, Subject } from 'rxjs';


import Eev from 'eev';

export const e = new Eev();

export default e;

@Component({
  selector: 'my-app-app1-test1',
  templateUrl: './app1-test1.component.html',
  styleUrls: ['./app1-test1.component.css']
})
export class App1Test1Component implements OnInit {
  singleSpaProps: SingleSpaProps = null;
  subscription: Subscription = null;
  authSubject: Observable<object> = null;

  message: string = '';

  constructor(
    private _router: Router,
    private _changeDetector: ChangeDetectorRef) { }

  ngOnInit() {
    this.subscription = singleSpaPropsSubject.subscribe(
      (props) => {
        this.singleSpaProps = props;
        // this.authSubject = props as unknown as Subject<object>;
        // if (this.authSubject && this.authSubject.subscribe) {
        //   this.authSubject.subscribe((token) => {
        //     debugger;
        //   })
        // }
        console.log(props);
        console.log(1);
      });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  navigate() {
    this._router.navigate(['/test2']);
  }

  ngAfterContentInit() {
    if (window['e']) {
      let eev = window['e'] as Eev;
      eev.on('global-search', message => {
        this.message = message;
        if (!this._changeDetector['destroyed']) {
          this._changeDetector.detectChanges();
        }
      });
    }
  }

}
