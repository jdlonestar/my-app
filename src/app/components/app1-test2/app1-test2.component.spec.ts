import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { App1Test2Component } from './app1-test2.component';

describe('App1Test2Component', () => {
  let component: App1Test2Component;
  let fixture: ComponentFixture<App1Test2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ App1Test2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(App1Test2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
