import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'my-app-app1-test2',
  templateUrl: './app1-test2.component.html',
  styleUrls: ['./app1-test2.component.css']
})
export class App1Test2Component implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  navigate() {
    this._router.navigate(['/test1']);
  }

}
