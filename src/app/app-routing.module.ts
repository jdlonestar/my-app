import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { App1Test1Component } from './components/app1-test1/app1-test1.component';
import { App1Test2Component } from './components/app1-test2/app1-test2.component';

const routes: Routes = [
  {
    path: 'test1',
    // loadChildren: './modules/test1.module#Test1Module',
    component: App1Test1Component
  },
  {
    path: 'test2',
    // loadChildren: './modules/test2.module#Test2Module',
    component: App1Test2Component
  },
  { path: '**', component: EmptyRouteComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [{ provide: APP_BASE_HREF, useValue: '/#/app1'}],
  exports: [RouterModule]
})
export class AppRoutingModule { }
