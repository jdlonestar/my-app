import { NgModule } from '@angular/core';

import { Test2RoutingModule } from './test2-routing.module';
import { App1Test2Component } from '../components/app1-test2/app1-test2.component';

@NgModule({
  declarations: [
    App1Test2Component,
  ],
  imports: [
    // Test2RoutingModule
  ],
  exports: [App1Test2Component],
  providers: [],
  bootstrap: []
})
export class Test2Module { }
