import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { App1Test1Component } from '../components/app1-test1/app1-test1.component';

const routes: Routes = [
  { path: '', component: App1Test1Component }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Test1RoutingModule { }
