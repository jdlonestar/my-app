import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { App1Test2Component } from '../components/app1-test2/app1-test2.component';

const routes: Routes = [
    {
      path: '',
      component: App1Test2Component,

      children: [],
    },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Test2RoutingModule { }
