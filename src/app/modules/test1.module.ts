import { NgModule } from '@angular/core';

import { Test1RoutingModule } from './test1-routing.module';
import { App1Test1Component } from '../components/app1-test1/app1-test1.component';

@NgModule({
  declarations: [
    App1Test1Component,
  ],
  imports: [
    // Test1RoutingModule
  ],
  entryComponents: [ App1Test1Component ],
  exports: [ App1Test1Component ],
  providers: [],
  bootstrap: []
})
export class Test1Module { }
