import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'my-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';

  constructor(private _router: Router) {}

  navigate() {
    this._router.navigate(['/test1'])
  }
}
