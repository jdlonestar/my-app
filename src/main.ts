import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
// import singleSpaAngular from 'single-spa-angular';
// import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
// import { NgZone } from '@angular/core';
// import { Router } from '@angular/router';
// import { AppModule } from './app/app.module';
// const lifecycles = singleSpaAngular({
//   bootstrapFunction: singleSpaProps => {
//     return platformBrowserDynamic().bootstrapModule(AppModule);
//   },
//   template: '<app-root />',
//   Router,
//   NgZone: NgZone,
// });
// export const bootstrap = lifecycles.bootstrap;
// export const mount = lifecycles.mount;
// export const unmount = lifecycles.unmount;